{shared{
open Eliom_lib
open Eliom_content
open Js
open Html5
open Html5.F
}}

{client{
open Oscrollbar
open Log
open JQuery
  }}

let main_service =
  Eliom_service.service ~path:["lwtscrollto"]
    ~get_params:Eliom_parameter.unit ()

{server{
   let ook =
     let rec ook_in s n = match n with
       | 0 -> s
       | n -> ook_in (s^"Ook ! ") (n - 1) in
     ook_in "" 1000
}}

{client{
    let pleaseLoad par =
      lwt _ = Scrollbar.add_scrollbar par in
      lwt _ = Scrollbar.lwt_scroll_to ~scroll:(`Bottom) par in
      log "Test one";
      ignore (Scrollbar.lwt_scroll_to ~scroll:(`Top) par);
      log "Test two";
      Lwt.return ()
  }}

let register_func home =
  (fun () () ->
     let paragraph = D.(div ~a:[a_id "monkey"; a_class ["content"]]
		          [(p [pcdata (ook)])]) in
     ignore{unit Lwt.t{pleaseLoad %paragraph}};
     Lwt.return
       (Eliom_tools.F.html
          ~title:"Scrollbar"
          ~css:[["css";"samples.css"];
                ["css";"jquery.mCustomScrollbar.css"]]
          ~js:[["js";"jquery-1.9.1.js"];
               ["js";"jquery.mCustomScrollbar.concat.min.js"]]
          Html5.F.(body [
	      h2 [pcdata "Example of use of lwt_on_scroll"];
       p [pcdata "Example of use of lwt_scroll_to. If you open firebug, you'll notice that test one is printed after the end of the scroll. That's because with \"lwt _ = ...\" we are waiting for the end of the scroll in lwt_scroll_to. Without it, lwt_scroll_to is equivalent to scroll_to (as shown with Test two)."];
       (p [(home)]);
       paragraph;
            ])))
