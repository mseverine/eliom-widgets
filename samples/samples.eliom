{shared{
open Eliom_lib
open Eliom_content
open Js
open Html5
open Html5.F
}}

{client{
open Oscrollbar
open Log
}}

module Scrollbar_app =
  Eliom_registration.App (
    struct
      let application_name = "samples"
    end)

let main_service =
  Eliom_service.service ~path:["scrollbar"] ~get_params:Eliom_parameter.unit ()

let l_scrolls = Html5.D.a  Scrolls.main_service [pcdata "Scrolls"] ()
let l_getdraggerpos = Html5.D.a Get_dragger_pos.main_service
    [pcdata "Get dragger pos"] ()
let l_lwtonscroll = Html5.D.a Lwt_on_scroll.main_service
    [pcdata "Scroll to and lwt"] ()

{server{
   let ook =
     let rec ook_in s n = match n with
       | 0 -> s
       | n -> ook_in (s^"Ook ! ") (n - 1) in
     ook_in "" 1000

}}

{client{
    let onload par =
      lwt _ = Scrollbar.add_scrollbar
          ~onScrollCallBack:(fun () -> log
                                ("Position : "^(string_of_int
                                                  (Scrollbar.get_dragger_pos
                                                   par))))
              par in
      log "Hello";
      Scrollbar.scrolls (fun () -> log ((string_of_int
                                           (Scrollbar.get_dragger_pct
                                            par))^" %"))
      par;
      lwt _ = Scrollbar.lwt_scroll_to ~scroll:(`Int 1000) par in
      log ("Position of the dragger : "^(string_of_int
                                           (Scrollbar.get_dragger_pos
                                            par)));
      Lwt.return ()
}}

let () =
  Scrollbar_app.register
    ~service:main_service
    (fun () () ->
       let paragraph = D.(div ~a:[a_id "mankey"; a_class ["samples"]]
		            [(p [l_scrolls]);
               (p [l_getdraggerpos]);
               (p [l_lwtonscroll])]) in
       ignore{unit Lwt.t{onload %paragraph}};
       Lwt.return
         (Eliom_tools.F.html
            ~title:"Scrollbar"
            ~css:[["css";"samples.css"];
                  ["css";"jquery.mCustomScrollbar.css"]]
            ~js:[["js";"jquery-1.9.1.js"];
                 ["js";"jquery.mCustomScrollbar.concat.min.js"]]
            Html5.F.(body [
	        h2 [pcdata "Examples of use of the scrollbar API"];
         paragraph
              ])));

  Scrollbar_app.register
    ~service:Lwt_on_scroll.main_service
    (Lwt_on_scroll.register_func
       (Html5.D.a main_service [pcdata "Home"] ()));

  Scrollbar_app.register
    ~service:Get_dragger_pos.main_service
    (Get_dragger_pos.register_func (Html5.D.a main_service
                                      [pcdata "Home"] ()));

  Scrollbar_app.register
    ~service:Scrolls.main_service
    (Scrolls.register_func (Html5.D.a main_service [pcdata "Home"] ()))
