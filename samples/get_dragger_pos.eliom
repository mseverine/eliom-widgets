{shared{
open Eliom_lib
open Eliom_content
open Js
open Html5
open Html5.F
}}

{client{
open JQuery
open Oscrollbar
open Log
}}

let main_service =
  Eliom_service.service ~path:["getdraggerpos"]
    ~get_params:Eliom_parameter.unit ()

{server{
   let ook =
     let rec ook_in s n = match n with
       | 0 -> s
       | n -> ook_in (s^"Ook ! ") (n - 1) in
     ook_in "" 1000
 }}

{client{
    let onload par =
      lwt _ = Scrollbar.add_scrollbar
          ~onScrollCallBack:(fun () -> log
                                ("Position : "^(string_of_int
                                                  (Scrollbar.get_dragger_pos
                                                     par)))) par in
      Scrollbar.scrolls (fun () -> log ((string_of_int
                                           (Scrollbar.get_dragger_pct
                                              par))^" %")) par;
      lwt _ = Scrollbar.lwt_scroll_to ~scroll:(`Int 1000) par in
      log ("Position of the dragger : "^(string_of_int
                                           (Scrollbar.get_dragger_pos par)));
      Lwt.return ()
  }}

let register_func home =
  (fun () () ->
     let paragraph = D.(div ~a:[a_id "monkey"; a_class ["content"]]
		          [(p [pcdata (ook)])]) in
     ignore{unit Lwt.t{onload %paragraph}};
     Lwt.return
         (Eliom_tools.F.html
            ~title:"Scrollbar"
            ~css:[["css";"samples.css"];
                  ["css";"jquery.mCustomScrollbar.css"]]
            ~js:[["js";"jquery-1.9.1.js"];
                 ["js";"jquery.mCustomScrollbar.concat.min.js"]]
            Html5.F.(body [
	        h2 [pcdata "Example of use of get_dragger_pos and get_dragger_pct"];
         p [pcdata "Example of use of get_dragger_pos. It's pretty straightforward, just call get_dragger_pos elt, and you'll get the position of THE TOP of the dragger. get_dragger_pct works the same, but will give you the value as a percentage."];
         p [pcdata "The only tricky part is that the position is only updated at the end of a scroll, so if you call it during a scroll, the position you'll get will be the starting position of the dragger."];
         p [pcdata "Also, remember that it returns the position of THE TOP of the dragger, and therefore will not be equivalent to the position you might want to send your bar with scroll_to. For example, on this page the scrollbar is sent to 1000, but the dragger_pos is only 180."];
         (p [(home)]);
         paragraph
              ])))
