{shared{
open Eliom_lib
open Eliom_content
open Js
open Html5
open Html5.F
}}

{client{
open JQuery
open Oscrollbar
open Log
}}

let main_service =
  Eliom_service.service ~path:["scrolls"] ~get_params:Eliom_parameter.unit ()

{server{
   let ook x =
     let rec ook_in s n = match n with
       | 0 -> s
       | n -> ook_in (s^"Ook ! ") (n - 1) in
     ook_in "" x
  }}

{client{
   let onload par =
     lwt _ = Scrollbar.add_scrollbar
         ~onScrollCallBack:(fun () -> log "End of scroll") par in
     let b = Scrollbar.scrolls (fun () ->
         log "End of scroll, with a function which is going to be canceled.")
         par in
     Scrollbar.scrolls (fun () ->
         log "End of scroll, with a function added.")
       par;
     lwt _ = Scrollbar.lwt_scroll_to ~scroll:(`Bottom) par in
     log "Scrolled down";
     lwt _ = Scrollbar.lwt_scroll_to ~scroll:(`Top) par in
     log "Scrolled up";
     Lwt.cancel b;
     log "Function canceled";
     Lwt.return ()
}}

let register_func home =
    (fun () () ->
      let paragraph = D.(div ~a:[a_id "monkey"; a_class ["content"]]
		           [(p [pcdata (ook 1000)])]) in
       ignore{unit Lwt.t{onload %paragraph}};
       Lwt.return
         (Eliom_tools.F.html
            ~title:"Scrollbar"
            ~css:[["css";"samples.css"];
                  ["css";"jquery.mCustomScrollbar.css"]]
            ~js:[["js";"jquery-1.9.1.js"];
                 ["js";"jquery.mCustomScrollbar.concat.min.js"]]
            Html5.F.(body [
	        h2 [pcdata "Example of use of the scrolls function"];
         p [pcdata "Usage of scrolls. If you open firebug, you'll notice that during the first scroll, the second and third log function are not present. They are only added after the scroll to the top. After the second scroll, the second log function is canceled, and therefore will not be called during subsequent call, while the third one will be kept."];
         (p [(home)]);
         paragraph
              ])))
